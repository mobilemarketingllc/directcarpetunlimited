<div class="product-detail-layout-1">
<?php
global $post;
$flooringtype = $post->post_type; 
$brand = get_field('brand') ;
$sku = get_field('sku') ;
$image = get_field('swatch_image_link') ? get_field('swatch_image_link'):"http://placehold.it/168x123?text=No+Image"; 
	
	if(strpos($image , 's7.shawimg.com') !== false){
		if(strpos($image , 'http') === false){ 
		$image = "http://" . $image;
	}	
		$image = "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[300x300]&sink";
	}else{
		if(strpos($image , 'http') === false){ 
		$image = "https://" . $image;
	}	
		$image= "https://mobilem.liquifire.com/mobilem?source=url[".$image . "]&scale=size[600x400]&sink";
	}	
?>
	<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>">
		<div class="fl-post-content clearfix grey-back" itemprop="text">
					<div class="container">
				<div class="row">
					<div class="col-md-7 col-sm-12 product-swatch">   
						<?php 
							$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-images.php';
							include( $dir );
						?>
					</div>
					<div class="col-md-5 col-sm-12 product-box">
						<?php					
							$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-brand-logos.php';
							include_once( $dir );                
						?>
						<?php if(get_field('parent_collection')) { ?>
							<h4><?php the_field('parent_collection'); ?> </h4>
						<?php } ?>
						
						<?php if(get_field('collection')) { ?>
						<h2 class="fl-post-title" itemprop="name"><?php the_field('collection'); ?></h2>
						<?php } ?>

						<?php if(get_field('color')) { ?>
						<h1 class="fl-post-title" itemprop="name"><?php the_field('color'); ?></h1>
						<?php } ?>

						<?php /*?><h5>Style No. <?php the_field('style'); ?>, Color No. <?php the_field('color_no'); ?></h5><?php*/ ?>
						
						<div class="product-colors">
							<?php
								$familysku = get_post_meta($post->ID, 'collection', true);
								$args = array(
									'post_type'      => $flooringtype,
									'posts_per_page' => -1,
									'post_status'    => 'publish',
									'meta_query'     => array(
										array(
											'key'     => 'collection',
											'value'   => $familysku,
											'compare' => '='
										),
										array(
											'key' => 'swatch_image_link',
											'value' => '',
											'compare' => '!='
											)
									)
								);
								$the_query = new WP_Query( $args );
							?>
							<ul>
								<li class="color-count" style="font-size:14px;"><?php  echo $the_query ->found_posts; ?> Colors Available</li>
							</ul>
						</div>
						
						<div class="button-wrapper">
							<a href="/flooring-coupon/?keyword=<?php echo $_COOKIE['keyword']; ?>&brand=<?php echo $brand;?>" class="button alt">Get Coupon</a>
							<a href="/contact-us/" class="button">Contact Us</a>
							<a href="/flooring-financing/">Get Financing</a> 
							<!-- <span class="divider">&nbsp;|&nbsp;</span><a href="/schedule-appointment/">Schedule a Measurement</a> -->
						</div>
					</div>
				</div>
					<div class="clearfix"></div>
				<?php 
					$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-color-slider.php';
					include( $dir );
				?>
					<div class="clearfix"></div>
				<?php
					$dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/includes/product-attributes.php';
					include( $dir );
				?>
			</div>
			</div><!-- .fl-post-content -->
	</article>
</div>
<?php
			$title = get_the_title();

			$jsonld = array('@context'=>'https://schema.org/','@type'=>'Product','name'=> $title,'image'=>$image,'description'=>$title,'sku'=>$sku,'mpn'=>$sku,'brand'=>array('@type'=>'thing','name'=>$brand), 
			'offers'=>array('@type'=>'offer','priceCurrency'=>'USD','price'=>'00','priceValidUntil'=>''));
			?>
			<?php echo '<script type="application/ld+json">'.json_encode($jsonld).'</script>';	?>